package by.bsuir.sem5.mrzvis.lab3;

import com.sun.istack.internal.NotNull;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.File;
import java.net.URL;
import java.util.List;

import javax.swing.*;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
    private HopfieldNet net = new HopfieldNet();
    private ImagePanel inputPane;
    private ImagePanel netPane;
    private JPanel memPane;
    private MemoryLoader loader;

    public MainWindow(MemoryLoader loader) {
        super("МРЗвИС, лабораторная работа № 3");
        this.loader = loader;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(
                Toolkit.getDefaultToolkit().getScreenSize().width / 5 * 4,
                Toolkit.getDefaultToolkit().getScreenSize().height / 5 * 4));
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        createControlPane();
        createMemoryPane();
        createInputPane();
        createNetPane();

        setVisible(true);
        net.setMemory(loader.getWidth(), loader.getHeight(), loader.getMemory());
        net.setInput(inputPane.getImage());
    }

    private void createNetPane() {
        JPanel pane = new JPanel();
        netPane = new ImagePanel(inputPane.getImage(), false);
        netPane.setPreferredSize(new Dimension(500, 500));
        pane.add(netPane);
        this.add(pane, BorderLayout.EAST);
    }

    private void createInputPane() {
        JPanel pane = new JPanel();
        inputPane = new ImagePanel(randomImage(loader.getWidth(),
                loader.getHeight()), true);
        inputPane.setPreferredSize(new Dimension(500, 500));
        pane.add(inputPane);
        this.add(pane, BorderLayout.WEST);
    }

    private void createMemoryPane() {
        memPane = new JPanel();
        memPane.setLayout(new FlowLayout());
        memPane.add(new JLabel("<html><font size='7'>Memory:</font></html>"));
        List<Image> memory = loader.getMemory();
        for (Image aMemory : memory) {
            ImagePanel imPane = new ImagePanel(aMemory, false);
            imPane.setPreferredSize(new Dimension(160, 160));
            memPane.add(imPane);
        }
        this.add(memPane, BorderLayout.SOUTH);
    }

    private void createControlPane() {
        JPanel controlPane = new JPanel(new FlowLayout());
        JButton loadButt = new JButton("Load memory"), addButt = new JButton(
                "Add to memory"), randomButt = new JButton("Randomize input"), setButt = new JButton(
                "Set input"), stepButt = new JButton("Step");

        loadButt.addActionListener(e -> {
            JFileChooser fileopen = new JFileChooser();
            URL url = getClass().getResource("/memories");
            fileopen.setCurrentDirectory(new File(url.getPath()));
            int ret = fileopen.showOpenDialog(null);
            if (ret == JFileChooser.APPROVE_OPTION) {
                File file = fileopen.getSelectedFile();
                loader.load(file);
                update();
                revalidate();
                repaint();
            }
        });
        addButt.addActionListener((e) -> {
            loader.save(inputPane.getImage());
            ImagePanel imPane = new ImagePanel(inputPane.getImage(), false);
            imPane.setPreferredSize(new Dimension(160, 160));
            memPane.add(imPane);
            memPane.revalidate();
            memPane.repaint();
            net.setMemory(loader.getWidth(), loader.getHeight(),
                    loader.getMemory());
        });
        randomButt.addActionListener((e) -> {
            inputPane.setImage(randomImage(loader.getWidth(),
                    loader.getHeight()));
        });
        setButt.addActionListener((e) -> {
            netPane.setImage(inputPane.getImage());
            net.setInput(inputPane.getImage());
        });
        stepButt.addActionListener(e -> {
            netPane.setImage(net.step());
        });
        controlPane.add(loadButt);
        controlPane.add(addButt);
        controlPane.add(randomButt);
        controlPane.add(setButt);
        controlPane.add(stepButt);
        this.add(controlPane, BorderLayout.NORTH);
    }

    private void update() {
        getContentPane().removeAll();
        createMemoryPane();
        createInputPane();
        createNetPane();
        createControlPane();
        net.setMemory(loader.getWidth(), loader.getHeight(), loader.getMemory());
        net.setInput(inputPane.getImage());
    }

    private Image randomImage(int width, int height) {
        Image image = new Image(width, height);
        for (int i = 0; i < width * height; i++) {
            if (Math.random() * 2 - 1 >= 0) {
                image.vector[i] = 1;
            } else {
                image.vector[i] = -1;
            }
        }
        return image;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MemoryLoader loader = new MemoryLoader();
                File file = new File(getClass().getResource("/memories/memory").getPath());
                loader.load(file);
                new MainWindow(loader);
            }
        });
    }

}
