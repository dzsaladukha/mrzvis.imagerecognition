package by.bsuir.sem5.mrzvis.lab3;

import java.awt.Color;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class Tile extends JButton {

    public Tile(ImagePanel imagePanel, int index, boolean canModify) {

        setColor(imagePanel.getValue(index));
        setEnabled(canModify);
        if (canModify)
            this.addActionListener((e) -> {
                imagePanel.revertValue(index);
                setColor(imagePanel.getValue(index));
            });

    }

    private void setColor(int value) {
        if (value == 1) {
            setBackground(Color.BLACK);
        } else if (value == -1) {
            setBackground(Color.WHITE);
        } else {
            setBackground(Color.RED);
        }
    }
}
