package by.bsuir.sem5.mrzvis.lab3;

import java.awt.GridLayout;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ImagePanel extends JPanel {

    private boolean canModify = false;
    private Image image;

    public ImagePanel(Image image, boolean canModify) {
        this.setImage(image);
        this.canModify = canModify;
        update();
    }

    private void update() {
        removeAll();
        setLayout(new GridLayout(image.width, image.height));
        for (int i = 0; i < image.width * image.height; i++) {
            add(new Tile(this, i, canModify));
        }
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
        update();
        revalidate();
        repaint();
    }

    public int getValue(int index) {
        return image.vector[index];
    }

    public void revertValue(int index) {
        image.vector[index] *= -1;
    }
}
