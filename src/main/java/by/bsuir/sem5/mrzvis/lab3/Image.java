package by.bsuir.sem5.mrzvis.lab3;

public class Image {
    int[] vector;
    int width;
    int height;

    public Image(int width, int height) {
        this.width = width;
        this.height = height;
        vector = new int[width * height];
    }
}
