package by.bsuir.sem5.mrzvis.lab3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MemoryLoader {
    private int imWidth = 5;
    private int imHeight = 5;
    private List<Image> memory = new ArrayList<Image>();;

    public void load(File file) {

        if (file.exists())
            try {
                Scanner in = new Scanner(new FileInputStream(file), "UTF-8");
                try {
                    String str = in.nextLine();
                    String[] tmp = str.split(" ");
                    this.imWidth = Integer.parseInt(tmp[0]);
                    this.imHeight = Integer.parseInt(tmp[1]);
                    memory.clear();

                    while (in.hasNext()) {
                        str = in.nextLine();
                        tmp = str.split(" ");
                        Image image = new Image(imWidth, imHeight);
                        for (int i = 0; i < imWidth * imHeight; i++) {
                            image.vector[i] = Integer.parseInt(tmp[i]);
                        }
                        memory.add(image);
                    }
                } finally {
                    in.close();
                }
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
    }

    public void save(Image addingImage) {
        Image im = new Image(imWidth, imHeight);
        im.vector = addingImage.vector.clone();
        memory.add(im);
        try {
            OutputStreamWriter out = new OutputStreamWriter(
                    new FileOutputStream("memories/memory"), "UTF-8");
            out.write(imWidth + " " + imHeight + "\n");
            for (Image image : memory) {
                String tmp = "";
                for (int j = 0; j < image.vector.length; j++) {
                    tmp += image.vector[j] + " ";
                }
                tmp += "\n";
                System.out.println(tmp);
                out.write(tmp);
            }
            out.close();
        } catch (Exception e) {
            System.err.println("Failed to save memory");
        }
    }

    public int getWidth() {
        return imWidth;
    }

    public int getHeight() {
        return imHeight;
    }

    public List<Image> getMemory() {
        return memory;
    }

}
