package by.bsuir.sem5.mrzvis.lab3;

import java.util.List;

import Jama.Matrix;

public class HopfieldNet {
    private Matrix weights;
    private int width;
    private int height;
    private Matrix input;

    public void setInput(Image image) {
        double[][] arr = new double[width * height][1];
        int index = 0;
        for (int value : image.vector) {
            arr[index++][0] = value;
        }
        input = new Matrix(arr);
    }

    public Image step() {
        input = sign(weights.times(input));

        Image image = new Image(width, height);
        double[][] arr = input.getArrayCopy();
        for (int index = 0; index < image.vector.length; index++) {
            image.vector[index] = (int) arr[index][0];
        }
        return image;
    }

    private Matrix sign(Matrix x) {
        for (int i = 0; i < x.getRowDimension(); i++) {
            if (x.get(i, 0) < 0) {
                x.set(i, 0, -1);
            } else if (x.get(i, 0) > 0) {
                x.set(i, 0, 1);
            }
        }
        return x;
    }

    // Training
    public void setMemory(int width, int height, List<Image> memory) {
        this.width = width;
        this.height = height;
        weights = new Matrix(width * height, width * height, 0);

        for (Image image : memory) {
            double[][] arr = new double[width * height][1];
            int j = 0;
            for (int value : image.vector) {
                arr[j++][0] = value;
            }

            Matrix x = new Matrix(arr);
            Matrix second = weights.times(x).minus(x);
            Matrix third = second.transpose();
            Matrix matrixThree = x.transpose().times(x)
                    .minus(x.transpose().times(weights).times(x));
            double first = 1 / matrixThree.get(0, 0);
            Matrix dW = second.times(third).times(first);
            weights = weights.plus(dW);

        }
        
        
        
//         int index = 0;
//         double[][] arr = new double[width * height][memory.size()];
//         for (Image image : memory) {
//         int j = 0;
//         for (int value : image.vector) {
//         arr[j++][index] = value;
//         }
//         index++;
//         }
//         Matrix x = new Matrix(arr);
//         weights = x.times(x.inverse());// use pseoduinverse
         
         
        // Matrix xt = x.transpose();
        // weights = x.times(xt.times(x).inverse()).times(xt);
    }
}
